package huyiming.com.schoolexpert.Adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;

import java.util.ArrayList;

import huyiming.com.schoolexpert.R;

/**
 * Created by huyiming on 15/7/30.
 */
public class homePageAdapter extends RecyclerView.Adapter<homePageAdapter.ViewHolder>{

    private ArrayList<AVObject> lessonList;
    private Context mContext;

    homePageAdapter(Context context, ArrayList<AVObject> list){
        mContext = context;
        lessonList = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item_people,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

            holder.lessonName.setText(lessonList.get(position).getString("lessonName"));
            holder.expertName.setText(lessonList.get(position).getString("expertName"));
            holder.appointmentCount.setText(lessonList.get(position).getString("appointmentCount"));
            holder.introduction.setText(lessonList.get(position).getString("introduction"));

    }

    @Override
    public int getItemCount() {
        return lessonList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView lessonName;
        public TextView expertName;
        public TextView appointmentCount;
        public TextView introduction;
        public ViewHolder(View view){
            super(view);
            lessonName = (TextView) view.findViewById(R.id.lesson_name);
            expertName = (TextView) view.findViewById(R.id.expertName);
            appointmentCount = (TextView) view.findViewById(R.id.appointmentCount);
            introduction = (TextView) view.findViewById(R.id.people_introduction);
        }
    }
}
