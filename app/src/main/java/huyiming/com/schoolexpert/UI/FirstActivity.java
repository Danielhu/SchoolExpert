package huyiming.com.schoolexpert.UI;


import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avos.avoscloud.AVAnalytics;

import huyiming.com.schoolexpert.R;
import huyiming.com.schoolexpert.UI.FirstPage.mainFragment;
import huyiming.com.schoolexpert.Utils.UIutil;


public class FirstActivity extends AppCompatActivity {

    Toolbar toolbar;
    CharSequence mtitle;
    DrawerLayout drawerLayout;
    ViewPager mViewPager;
    String currentSearchTip ;
    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);


        //record the times app opened
        AVAnalytics.trackAppOpened(getIntent());


        //Init toolBar
        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(null != actionBar){

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

        }

        toolbar.setPadding(0, getStatusbarHeight(), 0, 0);


        //init fragment
        mtitle = getTitle();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);

                drawerLayout.closeDrawers();
                return true;
            }
        });

        View header = LayoutInflater.from(this).inflate(R.layout.drawer_header,null);
        ImageView userDefaultHeader = (ImageView)header.findViewById(R.id.user_header);

        Bitmap origin = BitmapFactory.decodeResource(this.getResources(), R.drawable.user_head_default);
        Bitmap circle = UIutil.convertToCircle(origin);

        userDefaultHeader.setImageBitmap(circle);

        navigationView.addHeaderView(header);

        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open,
                R.string.drawer_close);
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();


    }


    private int getStatusbarHeight(){
        int result = 0;
        int barId = Resources.getSystem().getIdentifier("status_bar_height", "dimen", "android");
        if (barId > 0) {
            result = getResources().getDimensionPixelSize(barId);
        }
        return result;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (null != newText && newText.length() > 0) {
                    currentSearchTip = newText;

                }
                return true;
            }
        });



        return true;
    }

    public void showSearchTips(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mtitle = getString(R.string.curriculum);
                break;
            case 2:
                mtitle = getString(R.string.collect);
                break;
            case 3:
                mtitle = getString(R.string.beExpert);
                break;
        }
    }




}
