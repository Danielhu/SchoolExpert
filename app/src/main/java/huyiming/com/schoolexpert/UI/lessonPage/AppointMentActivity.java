package huyiming.com.schoolexpert.UI.lessonPage;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

import huyiming.com.schoolexpert.Presenter.LessonPreseter;
import huyiming.com.schoolexpert.R;
import huyiming.com.schoolexpert.UI.MaterialView.RevealBackgroundView;

public class AppointMentActivity extends AppCompatActivity {

    //@InjectView(R.id.vRevealBackground)
    RevealBackgroundView vRevealBackground;

    CoordinatorLayout root;

    //@InjectView(R.id.lessonAppBar)
    AppBarLayout appBarLayout;

    //@InjectView(R.id.lesson_background)
    ImageView imageView;

    //@InjectView(R.id.lesson_infoCard)
    LinearLayout linearLayout;

    FloatingActionButton floatingActionButton;

    LessonPreseter lessonPreseter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoint_ment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appoint_ment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
