package huyiming.com.schoolexpert.UI.lessonPage;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kale.activityoptions.transition.TransitionCompat;

import huyiming.com.schoolexpert.Presenter.LessonPreseter;
import huyiming.com.schoolexpert.R;
import huyiming.com.schoolexpert.UI.BaseActivity;
import huyiming.com.schoolexpert.UI.InfoView;

public class LessonActivity extends BaseActivity implements InfoView {

    public static final String lessonId= "lessonId";


    LessonPreseter lessonPreseter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String lessonId = getIntent().getStringExtra(LessonActivity.lessonId);
        lessonPreseter = new LessonPreseter(this,this,lessonId);

        setContentView(R.layout.activity_lesson);

        TransitionCompat.startTransition(this, R.layout.activity_lesson);


        setUpToolBar(R.id.lesson_tool_bar);
        View view = findViewById(R.id.content_root);
        lessonPreseter.setUpViews(view);
    }


    @Override
    protected void setUpToolBar(int toolBarId) {
        //Init toolBar
        toolbar = (Toolbar)findViewById(toolBarId);
        mtitle = "课程详情";
        toolbar.setTitle(mtitle);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(null != actionBar){

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);


        }
    }

    @Override
    public void refreshViewInfo(Object object) {

    }

    @Override
    public void onFinishRefresh() {

    }

    @Override
    public void getViewInfo() {

    }
}
